# Fda Demo

## How to run locally

To run locally, in the app root, execute:

```shell
./gradlew bootRun -Dspring.profiles.active=local
```

## How to run unit tests

To run unit tests, in the app root, execute:

```shell
./gradlew test
```

## How to run integraiton tests tests

* Run the application (for instance with `./gradlew bootRun -Dspring.profiles.active=local`)
* Once the app is up, use `./gradlew integrationTest`
* You can change the base path of tested app in `src/integrationTest/resources/application.properties`
* Or by specifying `./gradlew integrationTest -Dapi.url=<SOME_URL>`

## How to discover the API

* Once the app is running, please see `http://localhost:8081/swagger-ui/index.html`
* Also, I enabled actuator features which are not listed in swagger (since they are not part of the public API), but two
  feature I would like to point out are:
    * ```curl --location --request GET 'http://localhost:8081/actuator/prometheus'```
    * ```curl --location --request GET 'http://localhost:8081/actuator/info'```

## Production

* *WARNING* If not using h2, overriding `spring.jpa.database-platform` and `spring.datasource.driverClassName` in
  `application-prod.properties` is required
* Build the package `./gradlew bootJar`
* Copy `./build/libs/example-0.0.1-SNAPSHOT.jar` to target server directory
* run with:
  ```
     java -jar -Dspring.profiles.active=prod example-0.0.1-SNAPSHOT.jar \
               -Dspring.datasource.url=<your_url>
               -Dspring.datasource.username=<your_username>
               -Dspring.datasource.password=<your_password>
  ```
