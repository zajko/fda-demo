package com.riversoft.fda.example.app.business.fdaapplication.command;

import com.riversoft.fda.example.app.client.FdaClient;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.riversoft.fda.example.mother.FdaApplicationsSearchResponseMother.response;
import static com.riversoft.fda.example.mother.SearchFdaApplicationsRequestMother.validSearchRequest;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SearchFdaApplicationsCommandImplTest {

    @Mock
    private FdaClient fdaClient;

    @InjectMocks
    private SearchFdaApplicationsCommandImpl underTest;

    @Test
    public void whenExecuteCalledThenPassesDataToFdaClientAndReturnsItsReturnValue() {
        val expectedOutput = response();
        when(fdaClient.searchSubmittedApplications(any())).thenReturn(expectedOutput);

        val execute = underTest.execute(validSearchRequest());

        assertThat(execute).isEqualTo(expectedOutput);
        verify(fdaClient).searchSubmittedApplications(validSearchRequest());
    }
}