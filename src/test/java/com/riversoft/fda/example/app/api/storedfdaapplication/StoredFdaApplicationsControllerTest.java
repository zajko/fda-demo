package com.riversoft.fda.example.app.api.storedfdaapplication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.riversoft.fda.example.app.business.storedfdaapplication.command.CreateStoredFdaApplication;
import com.riversoft.fda.example.app.business.storedfdaapplication.command.DeleteStoredFdaApplication;
import com.riversoft.fda.example.app.business.storedfdaapplication.command.GetStoredFdaApplication;
import com.riversoft.fda.example.app.business.storedfdaapplication.command.ListStoredFdaApplication;
import com.riversoft.fda.example.app.error.FdaApplicationErrorResponse;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.riversoft.fda.example.app.error.ExampleApplicationException.alreadyExistsException;
import static com.riversoft.fda.example.app.error.FdaApplicationErrorResponse.alreadyExistsResponse;
import static com.riversoft.fda.example.mother.CreateStoredFdaApplicationRequestMother.createStoredFdaApplicationRequest;
import static com.riversoft.fda.example.mother.StoredFdaApplicationMother.storedFdaApplication;
import static com.riversoft.fda.example.mother.StoredFdaApplicationResponseMother.storedFdaApplicationResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(StoredFdaApplicationsController.class)
class StoredFdaApplicationsControllerTest {
    @MockBean
    private CreateStoredFdaApplication command;
    @MockBean
    private GetStoredFdaApplication getCommand;
    @MockBean
    private ListStoredFdaApplication listCommand;
    @MockBean
    private DeleteStoredFdaApplication deleteCommand;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mvc;

    @Test
    public void givenApplicationIdWhenCommandReturnsSuccessfullRequestThenReturnsResponse() throws Exception {
        val mockStoredFdaApplication = storedFdaApplication();
        when(command.execute(any())).thenReturn(mockStoredFdaApplication);
        val expectedOutput = storedFdaApplicationResponse();
        val request = objectMapper.writeValueAsString(createStoredFdaApplicationRequest());

        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.post("/api/v1.0/stored-fda-applications")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isOk());
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        val responseDeserialized = objectMapper
                .readValue(contentAsString, StoredFdaApplicationResponse.class);

        assertThat(responseDeserialized).isEqualTo(expectedOutput);
        verify(command).execute(createStoredFdaApplicationRequest().getApplicationId());
    }

    @Test
    public void givenApplicationIdWhenCommandThrowsExceptionThenReturnsErrorResponse() throws Exception {
        val expectedResponse = alreadyExistsResponse("Application id id-1 already created");
        when(command.execute(any())).thenThrow(alreadyExistsException("id-1"));
        val request = objectMapper.writeValueAsString(createStoredFdaApplicationRequest());

        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.post("/api/v1.0/stored-fda-applications")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isConflict());
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        val responseDeserialized = objectMapper
                .readValue(contentAsString, FdaApplicationErrorResponse.class);

        assertThat(responseDeserialized).isEqualTo(expectedResponse);
        verify(command).execute(createStoredFdaApplicationRequest().getApplicationId());
    }

    // List, get endpoints omitted for brevity
}