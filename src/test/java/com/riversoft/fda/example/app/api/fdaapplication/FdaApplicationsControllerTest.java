package com.riversoft.fda.example.app.api.fdaapplication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.riversoft.fda.example.Constants;
import com.riversoft.fda.example.app.business.fdaapplication.command.SearchFdaApplicationsCommand;
import com.riversoft.fda.example.app.client.dto.FdaApplicationsSearchResponse;
import com.riversoft.fda.example.app.error.ErrorResponse;
import com.riversoft.fda.example.app.error.ExampleApplicationException;
import com.riversoft.fda.example.app.error.FdaApplicationErrorResponse;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.riversoft.fda.example.mother.FdaApplicationsSearchResponseMother.response;
import static com.riversoft.fda.example.mother.SearchFdaApplicationsRequestMother.validSearchRequest;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(FdaApplicationsController.class)
class FdaApplicationsControllerTest {

    @MockBean
    SearchFdaApplicationsCommand command;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mvc;

    @Test
    public void givenSearchApplicationsInvokedThenReturnsDataFromCommand() throws Exception {
        val expectedOutput = response();
        when(command.execute(any())).thenReturn(expectedOutput);

        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.get("/api/v1.0/fda-applications")
                        .queryParam("fdaManufacturerName", "DEF"))
                .andExpect(status().isOk());
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        val responseDeserialized = objectMapper
                .readValue(contentAsString, FdaApplicationsSearchResponse.class);

        assertThat(responseDeserialized).isEqualTo(expectedOutput);
        verify(command).execute(validSearchRequest());
    }

    @Test
    public void givenSearchApplicationsInvokedWhenFdaClientExceptionThrowsThenReturnsErrorResponse() throws Exception {
        val errorResponse = new FdaApplicationErrorResponse(new ErrorResponse("som-code", "some-mesage"));
        when(command.execute(any())).thenThrow(new ExampleApplicationException("x", HttpStatus.NOT_FOUND, errorResponse));

        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.get("/api/v1.0/fda-applications")
                        .queryParam("fdaManufacturerName", "DEF"))
                .andExpect(status().isNotFound());
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        val responseDeserialized = objectMapper
                .readValue(contentAsString, FdaApplicationErrorResponse.class);

        assertThat(responseDeserialized).isEqualTo(errorResponse);
        verify(command).execute(validSearchRequest());
    }

    @Test
    public void givenSearchApplicationsInvokedWhenNoSearchManufacturerProvidedThenReturnsError() throws Exception {
        val res = response();
        when(command.execute(any())).thenReturn(res);

        mvc.perform(MockMvcRequestBuilders.get("/api/v1.0/fda-applications"))
                .andExpect(status().isBadRequest());
        //Specific error response checks omitted for brevity
    }

    @Test
    public void givenSearchApplicationsInvokedWhenLimitIsNegativeThenReturnsError() throws Exception {
        val res = response();
        when(command.execute(any())).thenReturn(res);

        mvc.perform(MockMvcRequestBuilders.get("/api/v1.0/fda-applications")
                        .queryParam("fdaManufacturerName", "DEF")
                        .queryParam("limit", "-1"))
                .andExpect(status().isBadRequest());
        //Specific error response checks omitted for brevity
    }

    @Test
    public void givenSearchApplicationsInvokedWhenLimitIsMoreThanMaxThenReturnsError() throws Exception {
        val res = response();
        when(command.execute(any())).thenReturn(res);

        mvc.perform(MockMvcRequestBuilders.get("/api/v1.0/fda-applications")
                        .queryParam("fdaManufacturerName", "DEF")
                        .queryParam("limit", "" + (Constants.MAX_PER_PAGE + 1)))
                .andExpect(status().isBadRequest());
        //Specific error response checks omitted for brevity
    }
}