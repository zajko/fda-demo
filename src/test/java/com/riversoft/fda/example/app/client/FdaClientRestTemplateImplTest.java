package com.riversoft.fda.example.app.client;

import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.net.URI;

import static com.riversoft.fda.example.mother.FdaApplicationsSearchResponseMother.response;
import static com.riversoft.fda.example.mother.SearchFdaApplicationsRequestMother.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FdaClientRestTemplateImplTest {

    @Mock
    private SearchFdaApplicationFetcher fetcher;
    @Mock
    private FdaApplicationFetcher fdaApplicationFetcher;
    private FdaClientRestTemplateImpl underTest;

    @Test
    public void givenRequestWithNullFdaManufacturerNameWhenSearchingFdaApplicationsThenReturnsError() {
        underTest = buildTemplateImpl("http://example-url.com");

        assertThatThrownBy(() -> {
            underTest.searchSubmittedApplications(noManufacturerRequest());
        })
                .isInstanceOf(AssertionError.class);
    }

    @Test
    public void givenRequestWithBlankFdaManufacturerNameWhenSearchingFdaApplicationsThenReturnsError() {
        underTest = buildTemplateImpl("http://example-url.com");

        assertThatThrownBy(() -> {
            underTest.searchSubmittedApplications(blankManufacturerRequest());
        })
                .isInstanceOf(AssertionError.class);
    }

    @Test
    public void givenValidRequestWhenSearchingFdaApplicationsThenReturnsPage() {
        underTest = buildTemplateImpl("http://example-url.com");
        when(fetcher.fetchForUrl(any())).thenReturn(response());

        val fdaApplicationResponsePage = underTest.searchSubmittedApplications(validSearchRequest());

        verify(fetcher).fetchForUrl(URI.create("http://example-url.com?search=openfda.manufacturer_name:%22DEF%22"));
        assertThat(fdaApplicationResponsePage).isEqualTo(response());
    }

    @Test
    public void givenRequestWithLimitWhenSearchingFdaApplicationsThenPassesLimit() {
        underTest = buildTemplateImpl("http://example-url.com");
        when(fetcher.fetchForUrl(any())).thenReturn(response());

        val fdaApplicationResponsePage = underTest.searchSubmittedApplications(withLimit());

        verify(fetcher).fetchForUrl(URI.create("http://example-url.com?search=openfda.manufacturer_name:%22DEF%22&limit=541"));
        assertThat(fdaApplicationResponsePage).isEqualTo(response());
    }

    @Test
    public void givenRequestWithLimitWhenSearchingFdaApplicationsThenPassesSkip() {
        underTest = buildTemplateImpl("http://example-url.com");
        when(fetcher.fetchForUrl(any())).thenReturn(response());

        val fdaApplicationResponsePage = underTest.searchSubmittedApplications(withSkip());

        verify(fetcher).fetchForUrl(URI.create("http://example-url.com?search=openfda.manufacturer_name:%22DEF%22&skip=50"));
        assertThat(fdaApplicationResponsePage).isEqualTo(response());
    }

    @Test
    public void givenRequestWithBrandNameLimitWhenSearchingFdaApplicationsThenPassesBrandName() {
        underTest = buildTemplateImpl("http://example-url.com");
        when(fetcher.fetchForUrl(any())).thenReturn(response());

        val fdaApplicationResponsePage = underTest.searchSubmittedApplications(withBrandName());

        verify(fetcher)
                .fetchForUrl(URI.create("http://example-url.com?search=openfda.manufacturer_name:%22DEF%22+AND+openfda.brand_name:%22Super%20brand%20name%22"));
        assertThat(fdaApplicationResponsePage).isEqualTo(response());
    }

    private FdaClientRestTemplateImpl buildTemplateImpl(String basePath) {
        return new FdaClientRestTemplateImpl(basePath, fetcher, fdaApplicationFetcher);
    }
}