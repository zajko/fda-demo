package com.riversoft.fda.example.mother;

import com.riversoft.fda.example.app.api.storedfdaapplication.StoredFdaApplicationResponse;

import static java.util.List.of;

public interface StoredFdaApplicationResponseMother {
    static StoredFdaApplicationResponse storedFdaApplicationResponse() {
        return StoredFdaApplicationResponse.builder()
                .id("xx")
                .manufacturerName(of("manuf-name"))
                .productNumbers(of("a", "b", "c"))
                .substanceName(of("subtance"))
                .build();
    }
}
