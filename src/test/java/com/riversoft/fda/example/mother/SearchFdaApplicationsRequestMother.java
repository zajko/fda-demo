package com.riversoft.fda.example.mother;

import com.riversoft.fda.example.app.api.fdaapplication.SearchFdaApplicationsRequest;

import java.util.Optional;

public interface SearchFdaApplicationsRequestMother {
    static SearchFdaApplicationsRequest validSearchRequest() {
        return SearchFdaApplicationsRequest.builder().fdaManufacturerName("DEF").build();
    }

    static SearchFdaApplicationsRequest withLimit() {
        return SearchFdaApplicationsRequest.builder().fdaManufacturerName("DEF").limit(Optional.of(541)).build();
    }

    static SearchFdaApplicationsRequest withSkip() {
        return SearchFdaApplicationsRequest.builder().fdaManufacturerName("DEF").skip(Optional.of(50)).build();
    }

    static SearchFdaApplicationsRequest withBrandName() {
        return SearchFdaApplicationsRequest.builder().fdaManufacturerName("DEF").fdaBrandName(Optional.of("Super brand name")).build();
    }

    static SearchFdaApplicationsRequest noManufacturerRequest() {
        return SearchFdaApplicationsRequest.builder().build();
    }

    static SearchFdaApplicationsRequest blankManufacturerRequest() {
        return SearchFdaApplicationsRequest.builder().fdaManufacturerName(" ").build();
    }
}
