package com.riversoft.fda.example.mother;

import com.riversoft.fda.example.app.api.storedfdaapplication.CreateStoredFdaApplicationRequest;

public interface CreateStoredFdaApplicationRequestMother {
    static CreateStoredFdaApplicationRequest createStoredFdaApplicationRequest() {
        return CreateStoredFdaApplicationRequest.builder().applicationId("abc").build();
    }
}
