package com.riversoft.fda.example.mother;

import com.riversoft.fda.example.app.business.storedfdaapplication.domain.StoredFdaApplication;
import lombok.val;

import static java.util.List.of;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public interface StoredFdaApplicationMother {

    static StoredFdaApplication storedFdaApplication(){
        val mockStoredFdaApplication = mock(StoredFdaApplication.class);
        when(mockStoredFdaApplication.getManufacturerName()).thenReturn(of("manuf-name"));
        when(mockStoredFdaApplication.getId()).thenReturn("xx");
        when(mockStoredFdaApplication.getSubstanceName()).thenReturn(of("subtance"));
        when(mockStoredFdaApplication.getProductNumbers()).thenReturn(of("a", "b", "c"));
        return mockStoredFdaApplication;
    }
}
