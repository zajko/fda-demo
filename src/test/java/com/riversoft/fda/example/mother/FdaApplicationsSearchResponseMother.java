package com.riversoft.fda.example.mother;

import com.riversoft.fda.example.app.client.dto.FdaApplicationResponse;
import com.riversoft.fda.example.app.client.dto.FdaApplicationsSearchResponse;
import com.riversoft.fda.example.app.client.dto.FdaApplicationsSearchResponse.FdaApplicationsSearchMetaResponse.FdaApplicationsSearchMetaResultsResponse;
import lombok.val;

import java.util.List;

import static com.riversoft.fda.example.app.client.dto.FdaApplicationsSearchResponse.FdaApplicationsSearchMetaResponse.builder;

public interface FdaApplicationsSearchResponseMother {

    static FdaApplicationsSearchResponse response() {
        val metaResults = FdaApplicationsSearchMetaResultsResponse.builder()
                .limit(100)
                .skip(10)
                .total(10000)
                .build();
        val meta = builder()
                .results(metaResults)
                .build();
        val singleResult = new FdaApplicationResponse("ABC1");
        val results = List.of(singleResult);
        return FdaApplicationsSearchResponse
                .builder()
                .meta(meta)
                .results(results)
                .build();
    }
}
