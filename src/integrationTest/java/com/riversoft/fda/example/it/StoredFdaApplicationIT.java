package com.riversoft.fda.example.it;

import com.riversoft.fda.example.app.error.ExampleApplicationException;
import com.riversoft.fda.example.it.util.ITBase;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static com.riversoft.fda.example.app.api.storedfdaapplication.CreateStoredFdaApplicationRequest.builder;
import static com.riversoft.fda.example.app.error.FdaApplicationErrorResponse.alreadyExistsResponse;
import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@Slf4j
public class StoredFdaApplicationIT extends ITBase {
    static final String EXAMPLE_APPLICATION_ID = "ANDA076433";

    @BeforeEach
    public void init() {
        //Clean the application if exists
        val maybeStoredFdaApplicationResponse = api.getStoredFdaApplication(EXAMPLE_APPLICATION_ID);
        maybeStoredFdaApplicationResponse
                .ifPresent(x -> api.deleteStoredFdaApplication(x.getId()));
    }

    @Test
    public void givenApplicationIdThatExistsInFdaWhenPostingThenCreatesInSystem() {
        api.createStoredFdaApplication(builder().applicationId(EXAMPLE_APPLICATION_ID).build());

        val maybeStored = api.getStoredFdaApplication(EXAMPLE_APPLICATION_ID);

        assertThat(maybeStored).isPresent();
        val stored = maybeStored.get();
        assertThat(stored.getManufacturerName()).isEqualTo(of("Rhodes Pharmaceuticals L.P."));
        assertThat(stored.getProductNumbers()).containsAll(of("001", "002"));
        assertThat(stored.getSubstanceName()).isEqualTo(of("FENOFIBRATE"));
    }

    @Test
    public void givenApplicationIdThatWasAlreadyStoredWhenPostingThenFails() {
        api.createStoredFdaApplication(builder().applicationId(EXAMPLE_APPLICATION_ID).build());

        assertThatThrownBy(() -> api.createStoredFdaApplication(builder().applicationId(EXAMPLE_APPLICATION_ID).build()))
                .isInstanceOf(ExampleApplicationException.class)
                .isEqualTo(alreadyExistsExceptionWithoutMessage(EXAMPLE_APPLICATION_ID));

    }

    public static ExampleApplicationException alreadyExistsExceptionWithoutMessage(String applicationNumber) {
        val msg = String.format("Application id %s already created", applicationNumber);
        return new ExampleApplicationException("", HttpStatus.CONFLICT, alreadyExistsResponse(msg));
    }
}
