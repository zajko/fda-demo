package com.riversoft.fda.example.it.util;

import org.junit.jupiter.api.BeforeEach;

public class ITBase {
    private static ITSpringContext context;
    static {
        context = new ITSpringContext();
        context.init();
    }

    protected ExampleApi api;

    @BeforeEach
    public void setupSuite() {
        api = new ExampleApi(BeanGetter.getBean(TestProps.class));
    }
}
