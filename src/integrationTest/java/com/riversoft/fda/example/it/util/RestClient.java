package com.riversoft.fda.example.it.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.riversoft.fda.example.app.error.ErrorResponse;
import com.riversoft.fda.example.app.error.ExampleApplicationException;
import com.riversoft.fda.example.app.error.FdaApplicationErrorResponse;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.val;
import org.springframework.http.*;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.ObjectUtils.isEmpty;

public class RestClient {
    private final String basePath;
    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    public RestClient(String basePath, ObjectMapper objectMapper) {
        this.basePath = basePath;
        val factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
        restTemplate = new RestTemplate(factory);
        this.objectMapper = objectMapper;
        this.objectMapper.disable(WRITE_DATES_AS_TIMESTAMPS);
        configureObjectMapper();
        restTemplate.setInterceptors(singletonList(new RequestResponseLoggingInterceptor()));
    }

    public RestClient(String basePath) {
        this(basePath, new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .registerModule(new JavaTimeModule()));
    }

    public <T> T post(String relativePath, Object request, Class<T> responseClass) {
        val url = join(basePath, relativePath);
        val entity = exchange(url, HttpMethod.POST, request, responseClass);
        return entity.getBody();
    }

    public <T> T get(String relativePath, Class<T> responseClass) {
        val url = join(basePath, relativePath);

        val entity = exchange(url, HttpMethod.GET, null, responseClass);
        return entity.getBody();
    }

    public void delete(String relativePath) {
        val url = join(basePath, relativePath);
        exchange(url, HttpMethod.DELETE, null, null);
    }

    private String join(String base, String... parts) {
        val b = new StringBuilder(base);
        for (String part : parts) {
            b.append("/").append(part.replaceAll("^\\+", ""));
        }
        return b.toString();
    }

    private <T> ResponseEntity<T> exchange(String path, HttpMethod method, Object data, Class<T> responseType) {
        try {
            val requestEntity = new HttpEntity<>(data);
            return restTemplate.exchange(path, method, requestEntity, responseType);
        } catch (RestClientResponseException ex) {
            throw mapResponse(ex);
        }
    }

    private RuntimeException mapResponse(RestClientResponseException ex) {
        if (isEmpty(ex.getResponseBodyAsByteArray())) {
            throw new NoResponseException(ex);
        }
        for (HttpMessageConverter<?> converter : restTemplate.getMessageConverters()) {
            if (converter.canRead(FdaApplicationErrorResponse.class, ex.getResponseHeaders().getContentType())) {
                try {
                    val response = ((HttpMessageConverter<FdaApplicationErrorResponse>) converter).read(FdaApplicationErrorResponse.class, new HttpInputMessage() {
                        @Override
                        public InputStream getBody() {
                            val responseBodyAsByteArray = ex.getResponseBodyAsByteArray();
                            if (responseBodyAsByteArray == null || responseBodyAsByteArray.length == 0) {
                                return new ByteArrayInputStream("{}".getBytes());
                            }
                            return new ByteArrayInputStream(responseBodyAsByteArray);
                        }

                        @Override
                        public HttpHeaders getHeaders() {
                            return ex.getResponseHeaders();
                        }
                    });
                    return new ExampleApplicationException("", HttpStatus.valueOf(ex.getRawStatusCode()), response);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return null;
    }

    private void configureObjectMapper() {
        val converterOpt = restTemplate.getMessageConverters()
                .stream()
                .filter(m -> m.getClass().getName().equals(MappingJackson2HttpMessageConverter.class.getName()))
                .map(m -> (MappingJackson2HttpMessageConverter) m)
                .findFirst();
        converterOpt.ifPresent(converter -> converter.setObjectMapper(this.objectMapper));
    }

    @Value
    @EqualsAndHashCode(callSuper = false)
    public static class ErrorResponseException extends RuntimeException {
        HttpStatus status;
        ErrorResponse response;
    }

    public static class NoResponseException extends RuntimeException {
        public NoResponseException(RuntimeException parent) {
            super(parent);
        }
    }
}
