package com.riversoft.fda.example.it.util;

import com.riversoft.fda.example.app.api.storedfdaapplication.CreateStoredFdaApplicationRequest;
import com.riversoft.fda.example.app.api.storedfdaapplication.StoredFdaApplicationResponse;
import com.riversoft.fda.example.app.error.ExampleApplicationException;

import java.util.Objects;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.Optional.ofNullable;

public class ExampleApi {
    private final RestClient client;

    public ExampleApi(TestProps props) {
        client = new RestClient(props.getApiUrl());
    }

    public Optional<StoredFdaApplicationResponse> getStoredFdaApplication(String applicationId) {
        try {
            return ofNullable(client.get(format("stored-fda-applications/%s", applicationId), StoredFdaApplicationResponse.class));
        } catch (ExampleApplicationException ex) {
            if (Objects.equals(ex.getResponse().getError().getCode(), "NOT_FOUND")) {
                return Optional.empty();
            }
            throw ex;
        } catch (Throwable t) {
            throw t;
        }
    }

    public StoredFdaApplicationResponse createStoredFdaApplication(CreateStoredFdaApplicationRequest request) {
        return client.post("stored-fda-applications", request, StoredFdaApplicationResponse.class);
    }

    public void deleteStoredFdaApplication(String applicationId) {
        client.delete(format("stored-fda-applications/%s", applicationId));
    }
}
