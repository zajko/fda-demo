package com.riversoft.fda.example.it.util;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;

@Slf4j
public class RequestResponseLoggingInterceptor implements ClientHttpRequestInterceptor {
    private int counter = 1;

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        logRequest(request, body);
        val response = execution.execute(request, body);
        logResponse(response);
        return response;
    }

    private void logRequest(HttpRequest request, byte[] body) throws IOException {
        log("\n===========================request " + counter + " begin================================================");
        log("URI         : %s", request.getURI());
        log("Method      : %s", request.getMethod());
        log("Headers     : %s", request.getHeaders());
        log("Request body: %s", new String(body, "UTF-8"));
        log("==========================request " + counter + " end================================================\n");
    }

    private void logResponse(ClientHttpResponse response) throws IOException {
        log("\n============================response " + counter + " begin==========================================");
        log("Status code  : %s", response.getStatusCode());
        log("Status text  : %s", response.getStatusText());
        log("Headers      : %s", response.getHeaders());
        try {
            log("Response body: %s", StreamUtils.copyToString(response.getBody(), Charset.defaultCharset()));
        } catch (RuntimeException ex) {
            log("Response body: <NONE>");
        }
        log("=======================response " + counter + " end=================================================\n");
        ++counter;
    }

    private void log(String msg, Object... params) {
        System.out.println(String.format(msg, params));
    }
}