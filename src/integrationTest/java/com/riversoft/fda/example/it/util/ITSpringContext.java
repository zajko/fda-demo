package com.riversoft.fda.example.it.util;

import lombok.Data;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@Data
public class ITSpringContext {
    public void init() {
        SpringApplication.run(ITSpringContext.class);
    }
}
