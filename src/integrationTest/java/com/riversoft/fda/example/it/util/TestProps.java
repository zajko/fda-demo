package com.riversoft.fda.example.it.util;

import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Accessors
@Getter
public class TestProps {
    @Value("${api.url}")
    private String apiUrl;
}
