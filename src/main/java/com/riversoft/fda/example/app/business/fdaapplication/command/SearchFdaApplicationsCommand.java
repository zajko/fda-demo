package com.riversoft.fda.example.app.business.fdaapplication.command;

import com.riversoft.fda.example.app.api.fdaapplication.SearchFdaApplicationsRequest;
import com.riversoft.fda.example.app.client.dto.FdaApplicationsSearchResponse;

public interface SearchFdaApplicationsCommand {
    FdaApplicationsSearchResponse execute(SearchFdaApplicationsRequest req);
}
