package com.riversoft.fda.example.app.business.storedfdaapplication.command;

import com.riversoft.fda.example.app.business.storedfdaapplication.domain.StoredFdaApplication;

public interface CreateStoredFdaApplication {
    StoredFdaApplication execute(String applicationId);
}
