package com.riversoft.fda.example.app.error;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;
import lombok.val;

import java.util.List;
import java.util.stream.Collectors;

@Value
@Jacksonized
@Builder
@AllArgsConstructor
public class FdaApplicationErrorResponse {
    ErrorResponse error;

    public static FdaApplicationErrorResponse notFoundResponse(String msg) {
        val errorResponse = FdaApplicationErrorResponse.builder()
                .error(new ErrorResponse("NOT_FOUND", msg))
                .build();
        return errorResponse;
    }

    public static FdaApplicationErrorResponse alreadyExistsResponse(String msg) {
        val errorResponse = FdaApplicationErrorResponse.builder()
                .error(new ErrorResponse("ALREADY_EXISTS", msg))
                .build();
        return errorResponse;
    }

    public static FdaApplicationErrorResponse validationFailedResponse(List<String> individualValidationExceptions) {
        val msg = individualValidationExceptions.stream().collect(Collectors.joining("; "));
        val errorResponse = FdaApplicationErrorResponse.builder()
                .error(new ErrorResponse("VALIDATION_FAILED", msg))
                .build();
        return errorResponse;
    }

}
