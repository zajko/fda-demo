package com.riversoft.fda.example.app.client;

import com.riversoft.fda.example.app.client.dto.FdaApplicationsSearchResponse;

import java.net.URI;

public interface SearchFdaApplicationFetcher {
    FdaApplicationsSearchResponse fetchForUrl(URI uri);
}
