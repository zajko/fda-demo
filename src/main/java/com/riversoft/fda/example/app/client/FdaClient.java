package com.riversoft.fda.example.app.client;

import com.riversoft.fda.example.app.api.fdaapplication.SearchFdaApplicationsRequest;
import com.riversoft.fda.example.app.client.dto.FdaApplicationResponse;
import com.riversoft.fda.example.app.client.dto.FdaApplicationsSearchResponse;

public interface FdaClient {
    FdaApplicationsSearchResponse searchSubmittedApplications(SearchFdaApplicationsRequest req);
    FdaApplicationResponse findByApplicationId(String applicationId);
}
