package com.riversoft.fda.example.app.business.storedfdaapplication.command;

import com.riversoft.fda.example.app.business.storedfdaapplication.domain.StoredFdaApplication;
import com.riversoft.fda.example.app.business.storedfdaapplication.domain.StoredFdaApplicationFactory;
import com.riversoft.fda.example.app.client.FdaClient;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@AllArgsConstructor
public class CreateStoredFdaApplicationImpl implements CreateStoredFdaApplication {
    private FdaClient fdaClient;
    private StoredFdaApplicationFactory factory;

    @Override
    @Transactional
    public StoredFdaApplication execute(String applicationId) {
        val byApplicationId = fdaClient.findByApplicationId(applicationId);
        return factory.createFromFdaResponse(byApplicationId);
    }
}
