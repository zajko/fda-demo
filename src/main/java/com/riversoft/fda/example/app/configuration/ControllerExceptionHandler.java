package com.riversoft.fda.example.app.configuration;

import com.riversoft.fda.example.app.error.ExampleApplicationException;
import lombok.val;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletResponse;

import static com.riversoft.fda.example.app.error.FdaApplicationErrorResponse.validationFailedResponse;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ControllerExceptionHandler {
    @ExceptionHandler({ExampleApplicationException.class})
    public Object handleException(ExampleApplicationException ex, HttpServletResponse response) {
        response.setStatus(NOT_FOUND.value());
        response.setContentType("application/json");
        response.setStatus(ex.getStatus().value());
        return new HttpEntity<Object>(ex.getResponse());
    }

    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Object methodArgumentNotValidException(MethodArgumentNotValidException ex) {
        val fieldErrors = ex.getBindingResult().getFieldErrors();
        val fieldExceptionExplanations = fieldErrors.stream().map(fieldError -> {
            val rejectedValue = fieldError.getRejectedValue();
            val rejectedValueAsString = rejectedValue == null ? null : rejectedValue.toString();
            return String.format("Field %s=%s failed (%s). Reason: %s",
                    fieldError.getField(),
                    rejectedValueAsString,
                    fieldError.getCode(),
                    fieldError.getDefaultMessage());
        }).collect(toList());
        return validationFailedResponse(fieldExceptionExplanations);
    }
}
