package com.riversoft.fda.example.app.client;

import com.riversoft.fda.example.app.client.dto.FdaApplicationResponse;

import java.net.URI;

public interface FdaApplicationFetcher {
    FdaApplicationResponse fetch(URI applicationIduri);
}
