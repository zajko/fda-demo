package com.riversoft.fda.example.app.business.storedfdaapplication.domain;

import com.riversoft.fda.example.app.business.storedfdaapplication.repository.StoredFdaApplicationEntity;
import com.riversoft.fda.example.app.business.storedfdaapplication.repository.StoredFdaApplicationRepository;
import com.riversoft.fda.example.app.util.DomainObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public class StoredFdaApplication extends DomainObject<StoredFdaApplicationEntity> {
    @Autowired
    private StoredFdaApplicationRepository repository;

    StoredFdaApplication(StoredFdaApplicationEntity entity) {
        super(entity);
    }

    public List<String> getSubstanceName() {
        return getEntity().getSubstanceName();
    }

    public List<String> getManufacturerName() {
        return getEntity().getManufacturerName();
    }

    public List<String> getProductNumbers() {
        return getEntity().getProductNumbers();
    }

    @Override
    protected JpaRepository<StoredFdaApplicationEntity, String> getRepository() {
        return repository;
    }
}
