package com.riversoft.fda.example.app.business.storedfdaapplication.command;

import com.riversoft.fda.example.app.business.storedfdaapplication.domain.StoredFdaApplication;
import com.riversoft.fda.example.app.business.storedfdaapplication.domain.StoredFdaApplicationFactory;
import com.riversoft.fda.example.app.error.ExampleApplicationException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@AllArgsConstructor
public class GetStoredFdaApplicationImpl implements GetStoredFdaApplication {
    private StoredFdaApplicationFactory factory;

    @Override
    @Transactional
    public StoredFdaApplication execute(String applicationId) {
        return factory.find(applicationId).orElseThrow(ExampleApplicationException::notFoundException);
    }
}
