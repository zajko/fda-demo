package com.riversoft.fda.example.app.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.riversoft.fda.example.app.client.dto.FdaApplicationsSearchResponse;
import com.riversoft.fda.example.app.error.ExampleApplicationException;
import com.riversoft.fda.example.app.error.FdaApplicationErrorResponse;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.http.HttpMethod;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.function.Supplier;

import static com.riversoft.fda.example.app.error.ExampleApplicationException.notFound;

@AllArgsConstructor
@Component
public class RestTemplateSearchFdaApplicationFetcher implements SearchFdaApplicationFetcher {
    private final RestTemplate template;
    private final ObjectMapper mapper;

    @Override
    @Retryable(maxAttempts = 4, backoff = @Backoff(delay = 500, multiplier = 2), exclude = ExampleApplicationException.class)
    public FdaApplicationsSearchResponse fetchForUrl(URI uri) {
        return executeAndRethrowApiException(() -> template
                .exchange(uri, HttpMethod.GET, null, FdaApplicationsSearchResponse.class)
                .getBody(), mapper);
    }

    public static <T> T executeAndRethrowApiException(Supplier<T> supplier, ObjectMapper mapper) {
        try {
            return supplier.get();
        } catch (HttpClientErrorException.NotFound notFound) {
            val rawResponse = notFound.getResponseBodyAsString();
            try {
                val errorResponse = mapper.readValue(rawResponse, FdaApplicationErrorResponse.class);
                throw notFound(errorResponse);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
