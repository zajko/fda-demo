package com.riversoft.fda.example.app.util;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class Page<T> {
    List<T> list;
    Integer skip;
    Integer total;
}
