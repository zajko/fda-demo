package com.riversoft.fda.example.app.client;

import com.riversoft.fda.example.app.api.fdaapplication.SearchFdaApplicationsRequest;
import com.riversoft.fda.example.app.client.dto.FdaApplicationResponse;
import com.riversoft.fda.example.app.client.dto.FdaApplicationsSearchResponse;
import lombok.val;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import static org.apache.logging.log4j.util.Strings.isNotBlank;

@Component
@lombok.Value
public class FdaClientRestTemplateImpl implements FdaClient {
    String basePath;
    SearchFdaApplicationFetcher searchFdaApplicationFetcher;
    FdaApplicationFetcher fdaApplicationFetcher;

    public FdaClientRestTemplateImpl(@Value("${com.riversoft.fda.example.fda-client.base-path}") String basePath,
                                     SearchFdaApplicationFetcher fetcher,
                                     FdaApplicationFetcher fdaApplicationFetcher) {
        this.basePath = basePath;
        this.searchFdaApplicationFetcher = fetcher;
        this.fdaApplicationFetcher = fdaApplicationFetcher;
    }


    @Override
    public FdaApplicationsSearchResponse searchSubmittedApplications(SearchFdaApplicationsRequest req) {
        assert (isNotBlank(req.getFdaManufacturerName()));
        val brandNameSuffix = req.getFdaBrandName()
                .map(brandName -> String.format("+AND+openfda.brand_name:\"%s\"", brandName))
                .orElse("");

        val search = String.format("openfda.manufacturer_name:\"%s\"", req.getFdaManufacturerName()) + brandNameSuffix;
        var builder = UriComponentsBuilder.fromHttpUrl(basePath)
                .queryParam("search", search);
        req.getLimit().ifPresent(limit -> builder.queryParam("limit", "" + limit));
        req.getSkip().ifPresent(skip -> builder.queryParam("skip", "" + skip));
        return searchFdaApplicationFetcher.fetchForUrl(builder.build().toUri());
    }

    @Override
    public FdaApplicationResponse findByApplicationId(String applicationId) {
        assert (isNotBlank(applicationId));
        val search = String.format("application_number:\"%s\"", applicationId);
        var builder = UriComponentsBuilder.fromHttpUrl(basePath)
                .queryParam("search", search);
        return fdaApplicationFetcher.fetch(builder.build().toUri());
    }
}
