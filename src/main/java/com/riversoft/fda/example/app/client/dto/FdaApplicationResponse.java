package com.riversoft.fda.example.app.client.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class FdaApplicationResponse {
    @JsonProperty("application_number")
    private String applicationNumber;
    private OpenFdaResponse openfda;
    private List<ProductResponse> products;
    private Map<String, Object> properties;

    public FdaApplicationResponse(String applicationNumber) {
        this();
        this.applicationNumber = applicationNumber;
    }
    public FdaApplicationResponse() {
        properties = new HashMap<>();
    }

    @JsonAnySetter
    public void add(String key, Object value) {
        properties.put(key, value);
    }

    @JsonAnyGetter
    public Map<String, Object> getProperties() {
        return properties;
    }
}
