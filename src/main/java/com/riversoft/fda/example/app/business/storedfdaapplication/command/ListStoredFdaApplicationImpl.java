package com.riversoft.fda.example.app.business.storedfdaapplication.command;

import com.riversoft.fda.example.app.business.storedfdaapplication.domain.StoredFdaApplication;
import com.riversoft.fda.example.app.business.storedfdaapplication.domain.StoredFdaApplicationFactory;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class ListStoredFdaApplicationImpl implements ListStoredFdaApplication {
    private StoredFdaApplicationFactory factory;

    @Override
    public List<StoredFdaApplication> execute() {
        return factory.list();
    }
}
