package com.riversoft.fda.example.app.api.fdaapplication;

import com.riversoft.fda.example.app.business.fdaapplication.command.SearchFdaApplicationsCommand;
import com.riversoft.fda.example.app.client.dto.FdaApplicationsSearchResponse;
import lombok.AllArgsConstructor;
import org.springframework.metrics.annotation.Timed;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1.0/fda-applications")
@AllArgsConstructor
@Timed
public class FdaApplicationsController {
    private final SearchFdaApplicationsCommand command;

    @GetMapping
    public FdaApplicationsSearchResponse searchApplications(@Valid SearchFdaApplicationsRequest req) {
        return command.execute(req);
    }
}
