package com.riversoft.fda.example.app.business.storedfdaapplication.domain;


import com.riversoft.fda.example.app.business.storedfdaapplication.repository.StoredFdaApplicationEntity;
import com.riversoft.fda.example.app.business.storedfdaapplication.repository.StoredFdaApplicationRepository;
import com.riversoft.fda.example.app.client.dto.FdaApplicationResponse;
import com.riversoft.fda.example.app.client.dto.OpenFdaResponse;
import com.riversoft.fda.example.app.client.dto.ProductResponse;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.riversoft.fda.example.app.error.ExampleApplicationException.alreadyExistsException;
import static java.util.Collections.emptyList;

@Component
@AllArgsConstructor
public class StoredFdaApplicationFactoryImpl implements StoredFdaApplicationFactory {
    private final StoredFdaApplicationRepository repository;

    @Override
    public StoredFdaApplication createFromFdaResponse(FdaApplicationResponse byApplicationId) {
        val applicationNumber = byApplicationId.getApplicationNumber();
        val byId = repository.findById(applicationNumber);
        if (byId.isPresent()) {
            throw alreadyExistsException(applicationNumber);
        }
        val maybeOpenFda = Optional.ofNullable(byApplicationId.getOpenfda());
        val manufacturerName = maybeOpenFda
                .map(OpenFdaResponse::getManufacturerName)
                .orElse(emptyList());
        val substanceName = maybeOpenFda
                .map(OpenFdaResponse::getSubstanceName)
                .orElse(emptyList());
        val products = Optional.ofNullable(byApplicationId.getProducts()).orElse(emptyList());
        val productNumbers = products
                .stream()
                .map(ProductResponse::getProductNumber)
                .collect(Collectors.toList());

        val entity = new StoredFdaApplicationEntity()
                .setId(applicationNumber)
                .setManufacturerName(manufacturerName)
                .setSubstanceName(substanceName)
                .setProductNumbers(productNumbers);

        return new StoredFdaApplication(entity);
    }

    @Override
    public Optional<StoredFdaApplication> find(String id) {
        return repository.findById(id).map(this::get);
    }

    @Override
    public List<StoredFdaApplication> list() {
        return repository.findAll().stream().map(this::get).collect(Collectors.toList());
    }

    @Override
    public void delete(String applicationId) {
        repository.deleteById(applicationId);
    }

    private StoredFdaApplication get(@NotNull StoredFdaApplicationEntity entity) {
        return new StoredFdaApplication(entity);
    }
}
