package com.riversoft.fda.example.app.business.storedfdaapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StoredFdaApplicationRepository extends JpaRepository<StoredFdaApplicationEntity, String> {
}
