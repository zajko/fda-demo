package com.riversoft.fda.example.app.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.riversoft.fda.example.app.client.dto.FdaApplicationResponse;
import com.riversoft.fda.example.app.client.dto.FdaApplicationsSearchResponse;
import com.riversoft.fda.example.app.error.ExampleApplicationException;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.http.HttpMethod;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

import static com.riversoft.fda.example.app.client.RestTemplateSearchFdaApplicationFetcher.executeAndRethrowApiException;

@AllArgsConstructor
@Component
public class FdaApplicationFetcherImpl implements FdaApplicationFetcher {
    private final RestTemplate template;
    private final ObjectMapper mapper;

    @Override
    @Retryable(maxAttempts = 4, backoff = @Backoff(delay = 500, multiplier = 2), exclude = ExampleApplicationException.class)
    public FdaApplicationResponse fetch(URI uri) {
        val response = executeAndRethrowApiException(() -> template
                .exchange(uri, HttpMethod.GET, null, FdaApplicationsSearchResponse.class)
                .getBody(), mapper);
        return response.getResults().get(0);
    }
}
