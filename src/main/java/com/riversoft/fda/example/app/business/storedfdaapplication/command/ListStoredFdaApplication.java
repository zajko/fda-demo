package com.riversoft.fda.example.app.business.storedfdaapplication.command;

import com.riversoft.fda.example.app.business.storedfdaapplication.domain.StoredFdaApplication;

import java.util.List;

public interface ListStoredFdaApplication {
    List<StoredFdaApplication> execute();
}
