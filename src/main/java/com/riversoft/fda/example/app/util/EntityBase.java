package com.riversoft.fda.example.app.util;

import java.time.ZonedDateTime;

public interface EntityBase {
    String getId();
    EntityBase setUpdatedAt(ZonedDateTime updatedAt);
}
