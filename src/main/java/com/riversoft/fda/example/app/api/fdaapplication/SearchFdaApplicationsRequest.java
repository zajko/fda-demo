package com.riversoft.fda.example.app.api.fdaapplication;

import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.Optional;

import static com.riversoft.fda.example.Constants.MAX_PER_PAGE;

@Value
@Builder(builderClassName = "Builder", toBuilder = true)
public class SearchFdaApplicationsRequest {
    @NotBlank
    String fdaManufacturerName;
    @lombok.Builder.Default
    Optional<String> fdaBrandName = Optional.empty();
    @lombok.Builder.Default
    Optional<@Min(0)  @Max(MAX_PER_PAGE) Integer> limit = Optional.empty();
    @lombok.Builder.Default
    Optional<@Min(0)  @Max(MAX_PER_PAGE) Integer> skip = Optional.empty();
}
