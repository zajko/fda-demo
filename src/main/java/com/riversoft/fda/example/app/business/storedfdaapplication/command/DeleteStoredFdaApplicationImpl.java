package com.riversoft.fda.example.app.business.storedfdaapplication.command;

import com.riversoft.fda.example.app.business.storedfdaapplication.domain.StoredFdaApplicationFactory;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@AllArgsConstructor
public class DeleteStoredFdaApplicationImpl implements DeleteStoredFdaApplication {
    private StoredFdaApplicationFactory factory;

    @Override
    @Transactional
    public void execute(String applicationId) {
        factory.delete(applicationId);
    }
}
