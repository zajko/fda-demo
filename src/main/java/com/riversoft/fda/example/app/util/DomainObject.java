package com.riversoft.fda.example.app.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import java.time.ZonedDateTime;

import static com.riversoft.fda.example.app.configuration.DependencyInjector.autowire;

public abstract class DomainObject<T extends EntityBase> {

    private T entity;
    @Autowired
    private EntityManager entityManager;

    public DomainObject(T entity) {
        this.entity = entity;
        autowire(this);
    }

    protected T save() {
        if (entityManager.contains(getEntity())) {
            entity.setUpdatedAt(ZonedDateTime.now());
        }
        return getRepository().save(entity);
    }

    public T getEntity() {
        return entity;
    }

    @PostConstruct
    public void init() {
        if (!entityManager.contains(getEntity())) {
            save();
        }
    }

    protected abstract JpaRepository<T, String> getRepository();

    public String getId() {
        return entity.getId();
    };

}
