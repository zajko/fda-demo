package com.riversoft.fda.example.app.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

@Data
@Jacksonized
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class FdaApplicationsSearchResponse {
    private final FdaApplicationsSearchMetaResponse meta;
    private final List<FdaApplicationResponse> results;

    @Data
    @Jacksonized
    @Builder
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class FdaApplicationsSearchMetaResponse {
        private final FdaApplicationsSearchMetaResultsResponse results;
        @Data
        @JsonIgnoreProperties(ignoreUnknown = true)
        @Jacksonized
        @Builder
        public static class FdaApplicationsSearchMetaResultsResponse {
            private final int skip;
            private final int limit;
            private final int total;
        }
    }
}
