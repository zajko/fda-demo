package com.riversoft.fda.example.app.api.storedfdaapplication;

import com.riversoft.fda.example.app.business.storedfdaapplication.command.CreateStoredFdaApplication;
import com.riversoft.fda.example.app.business.storedfdaapplication.command.DeleteStoredFdaApplication;
import com.riversoft.fda.example.app.business.storedfdaapplication.command.GetStoredFdaApplication;
import com.riversoft.fda.example.app.business.storedfdaapplication.command.ListStoredFdaApplication;
import com.riversoft.fda.example.app.business.storedfdaapplication.domain.StoredFdaApplication;
import lombok.AllArgsConstructor;
import org.springframework.metrics.annotation.Timed;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1.0/stored-fda-applications")
@AllArgsConstructor
@Timed
public class StoredFdaApplicationsController {
    private final CreateStoredFdaApplication createCommand;
    private final GetStoredFdaApplication getCommand;
    private final ListStoredFdaApplication listCommand;
    private final DeleteStoredFdaApplication deleteCommand;

    @PostMapping
    @Transactional
    public StoredFdaApplicationResponse create(@Valid @RequestBody CreateStoredFdaApplicationRequest req) {
        return map(createCommand.execute(req.getApplicationId()));
    }

    @GetMapping("/{id}")
    public StoredFdaApplicationResponse get(@PathVariable String id) {
        return map(getCommand.execute(id));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
        deleteCommand.execute(id);
    }

    @GetMapping
    public List<StoredFdaApplicationResponse> get() {
        return listCommand.execute().stream().map(this::map).collect(Collectors.toList());
    }

    private StoredFdaApplicationResponse map(StoredFdaApplication storedFdaApplication) {
        return StoredFdaApplicationResponse.builder()
                .substanceName(storedFdaApplication.getSubstanceName())
                .manufacturerName(storedFdaApplication.getManufacturerName())
                .productNumbers(storedFdaApplication.getProductNumbers())
                .id(storedFdaApplication.getId())
                .build();
    }
}
