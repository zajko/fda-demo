package com.riversoft.fda.example.app.api.storedfdaapplication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import javax.validation.constraints.NotBlank;

@Value
@Jacksonized
@Builder
public class CreateStoredFdaApplicationRequest {
    @NotBlank
    @JsonProperty("application_id")
    String applicationId;
}
