package com.riversoft.fda.example.app.business.fdaapplication.command;

import com.riversoft.fda.example.app.api.fdaapplication.SearchFdaApplicationsRequest;
import com.riversoft.fda.example.app.client.FdaClient;
import com.riversoft.fda.example.app.client.dto.FdaApplicationsSearchResponse;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Value
public class SearchFdaApplicationsCommandImpl implements SearchFdaApplicationsCommand {
    FdaClient fdaClient;

    @Override
    public FdaApplicationsSearchResponse execute(SearchFdaApplicationsRequest req) {
        return fdaClient.searchSubmittedApplications(req);
    }
}
