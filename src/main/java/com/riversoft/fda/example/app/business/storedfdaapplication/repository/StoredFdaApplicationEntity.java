package com.riversoft.fda.example.app.business.storedfdaapplication.repository;

import com.riversoft.fda.example.app.util.EntityBase;
import com.vladmihalcea.hibernate.type.json.JsonType;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.util.List;

@Entity
@Table(name = "stored_fda_applications")
@Setter
@Getter
@Accessors(chain = true)
@TypeDef(name = "json", typeClass = JsonType.class)
public class StoredFdaApplicationEntity implements EntityBase {
    @Id
    private String id;

    @Type(type = "json")
    @Column(name = "substance_name", columnDefinition = "json")
    private List<String> substanceName;

    @Type(type = "json")
    @Column(name = "manufacturer_name", columnDefinition = "json")
    private List<String> manufacturerName;

    @Type(type = "json")
    @Column(name = "product_numbers", columnDefinition = "json")
    private List<String> productNumbers;

    @CreatedDate
    @Column(name = "created_at")
    protected ZonedDateTime createdAt = ZonedDateTime.now();

    @LastModifiedDate
    @Column(name = "updated_at")
    protected ZonedDateTime updatedAt;
}