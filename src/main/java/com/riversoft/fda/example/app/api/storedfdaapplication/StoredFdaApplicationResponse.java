package com.riversoft.fda.example.app.api.storedfdaapplication;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

@Value
@Jacksonized
@Builder
public class StoredFdaApplicationResponse {
    String id;
    List<String> manufacturerName;
    List<String> substanceName;
    List<String> productNumbers;
}
