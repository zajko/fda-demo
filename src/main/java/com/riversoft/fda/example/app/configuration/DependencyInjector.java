package com.riversoft.fda.example.app.configuration;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@AllArgsConstructor
public class DependencyInjector {
    private static AutowireCapableBeanFactory staticAutowireCapableBeanFactory;
    private final AutowireCapableBeanFactory autowireCapableBeanFactory;

    @PostConstruct
    public void init() {
        staticAutowireCapableBeanFactory = autowireCapableBeanFactory;
    }

    public static <T> T autowire(T t) {
        staticAutowireCapableBeanFactory.autowireBean(t);
        return (T) staticAutowireCapableBeanFactory.initializeBean(t, t.getClass().getSimpleName());
    }
}
