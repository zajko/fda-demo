package com.riversoft.fda.example.app.business.storedfdaapplication.domain;

import com.riversoft.fda.example.app.client.dto.FdaApplicationResponse;

import java.util.List;
import java.util.Optional;

public interface StoredFdaApplicationFactory {

    StoredFdaApplication createFromFdaResponse(FdaApplicationResponse byApplicationId);

    Optional<StoredFdaApplication> find(String id);

    List<StoredFdaApplication> list();

    void delete(String applicationId);
}
