package com.riversoft.fda.example.app.business.storedfdaapplication.command;

public interface DeleteStoredFdaApplication {
    void execute(String applicationId);
}
