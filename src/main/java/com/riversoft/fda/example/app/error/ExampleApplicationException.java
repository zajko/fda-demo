package com.riversoft.fda.example.app.error;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import org.springframework.http.HttpStatus;

import static com.riversoft.fda.example.app.error.FdaApplicationErrorResponse.alreadyExistsResponse;
import static com.riversoft.fda.example.app.error.FdaApplicationErrorResponse.notFoundResponse;

@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper=false)
public class ExampleApplicationException extends RuntimeException {
    private HttpStatus status = HttpStatus.BAD_REQUEST;
    private FdaApplicationErrorResponse response;

    public ExampleApplicationException(String message, HttpStatus status, FdaApplicationErrorResponse response) {
        super(message);
        this.status = status;
        this.response = response;
    }

    public ExampleApplicationException(String message, FdaApplicationErrorResponse response) {
        super(message);
        this.response = response;
    }

    public static ExampleApplicationException notFoundException() {
        val msg = "No matches found!";
        return new ExampleApplicationException(msg, notFoundResponse(msg));
    }

    public static ExampleApplicationException alreadyExistsException(String applicationNumber) {
        val msg = String.format("Application id %s already created", applicationNumber);
        return new ExampleApplicationException(msg, HttpStatus.CONFLICT, alreadyExistsResponse(msg));
    }

    public static ExampleApplicationException notFound(FdaApplicationErrorResponse errorResponse) {
        val msg = "Error when trying to fetch fda response";
        return new ExampleApplicationException(msg, HttpStatus.NOT_FOUND, errorResponse);
    }
}
