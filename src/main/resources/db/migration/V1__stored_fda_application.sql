create table stored_fda_applications
(
    id                VARCHAR(1000) PRIMARY KEY,
    substance_name    JSON,
    manufacturer_name JSON,
    product_numbers   JSON,
    created_at        TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at        TIMESTAMP
)