plugins {
	id 'org.springframework.boot' version '2.5.6'
	id 'io.spring.dependency-management' version '1.0.11.RELEASE'
	id 'java'
}

ext {
	gitAbbRevision = 'git rev-parse --short HEAD'.execute().text.trim()
}

group = 'com.riversoft.fda'
version = '0.0.1-SNAPSHOT'
sourceCompatibility = '11'

def springfoxVersion = '3.0.0'

configurations {
	compileOnly {
		extendsFrom annotationProcessor
	}
}

repositories {
	mavenCentral()
}

dependencies {
	implementation 'org.springframework.boot:spring-boot-starter-actuator'
	implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
	implementation 'org.springframework.boot:spring-boot-starter-web'
	implementation 'org.springframework.boot:spring-boot-starter-aop'
	implementation 'org.springframework.boot:spring-boot-starter-validation'
	compileOnly 'org.projectlombok:lombok'
	annotationProcessor 'org.projectlombok:lombok'
	testCompileOnly 'org.projectlombok:lombok'
	testAnnotationProcessor 'org.projectlombok:lombok'
	testImplementation 'org.springframework.boot:spring-boot-starter-test'
	implementation group: 'org.flywaydb', name: 'flyway-core', version: '8.0.2'
}

test {
	useJUnitPlatform()
}

sourceSets {
	integrationTest {
		java {
			compileClasspath += main.output + test.output
			runtimeClasspath += main.output + test.output
 			srcDir file('src/integrationTest/java')
		}
		resources.srcDir file('src/integrationTest/resources')
	}
}

task integrationTest(type: Test) {
	description = 'Runs integration tests.'
	group = 'verification'
	testClassesDirs = sourceSets.integrationTest.output.classesDirs
	classpath = sourceSets.integrationTest.runtimeClasspath
	reports.html.destination = file("${reporting.baseDir}/${name}")
	useJUnitPlatform()
}

rootProject.tasks.named("processIntegrationTestResources") {
	duplicatesStrategy = 'include'
}

configurations {
	integrationTestImplementation.extendsFrom testImplementation
	integrationTestRuntimeOnly.extendsFrom testRuntime
	integrationTestAnnotationProcessor.extendsFrom testAnnotationProcessor
}

dependencies {
	integrationTestImplementation 'org.projectlombok:lombok:1.18.12'
	integrationTestImplementation group: 'com.opencsv', name: 'opencsv', version: '5.1'

	integrationTestRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:5.3.1'
	integrationTestImplementation group: 'org.assertj', name: 'assertj-core', version: '3.15.0'
	integrationTestImplementation 'org.junit.jupiter:junit-jupiter-api:5.3.1'
	integrationTestAnnotationProcessor(
			"org.projectlombok:lombok"
	)

	testImplementation('org.junit.jupiter:junit-jupiter:5.6.1')
	implementation group: 'org.springframework.metrics', name: 'spring-metrics', version: '0.5.1.RELEASE'
	implementation group: 'io.micrometer', name: 'micrometer-registry-prometheus', version: '1.7.5'
	implementation group: 'javax.validation', name: 'validation-api', version: '2.0.1.Final'
	implementation group: 'org.springframework.retry', name: 'spring-retry', version: '1.3.1'
	implementation group: 'com.h2database', name: 'h2', version: '1.4.200'
	implementation group: 'com.vladmihalcea', name: 'hibernate-types-52', version: '2.14.0'
	implementation "io.springfox:springfox-boot-starter:$springfoxVersion"
	implementation group: 'io.springfox', name: 'springfox-swagger2', version: "$springfoxVersion"
	implementation group: 'io.springfox', name: 'springfox-swagger-ui', version: "$springfoxVersion"
}

task getVersions(dependsOn: processResources) {
	doLast {
		def separator = System.getProperty('line.separator')
		def version = project.version.toString()
		def file = new File("$buildDir/resources/main/application.properties")
		file << "\ninfo.app.version=${version}$separator"
		file << "info.app.git_commit=${gitAbbRevision}$separator"
	}
}

classes {
	dependsOn getVersions
}

tasks.named('check') { dependsOn(test) }